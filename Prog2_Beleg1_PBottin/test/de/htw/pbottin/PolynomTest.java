/**
 * 
 */
package de.htw.pbottin;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Testklasse für die Polynom-Klasse.
 * 
 * @author Paul Bottin
 * 
 */
public class PolynomTest
{
  
  /*
   * Beispiel-Polynome
   */
  
  /** 4x^3 - 3x^2 + 2x - 1 */
  Polynom BEISPIEL_1;
  
  /** -4.0x^3 */
  Polynom BEISPIEL_2;
  
  /** 3.0x^2+x */
  Polynom BEISPIEL_3;
  
  /** 3.0x^2 - x */
  Polynom BEISPIEL_4;
  
  /** 1 */
  Polynom BEISPIEL_5;
  
  /** -1 */
  Polynom BEISPIEL_6;
  
  /** 0 */
  Polynom BEISPIEL_7;
  
  /** (1/3)x^3 - 2x^2 + 3x */
  Polynom BEISPIEL_8;
  
  /** x^2 - 4.0x + 3.0 */
  Polynom BEISPIEL_9;
  
  /** 2.0x - 4.0 */
  Polynom BEISPIEL_10;
  
  /** 3x^2 + 3x */
  Polynom BEISPIEL_11;
  
  /** -4.0x^3 + 3.0x^2 + x + 1.0 */
  Polynom BEISPIEL_12;
  
  /** -3.0x^3 - 5.0x^2 + 2.0x + 1.0 */
  Polynom BEISPIEL_13;
  
  /**
   * Test method for {@link de.htw.pbottin.Polynom#Polynom(double...)}.
   */
  @Before
  public void setUp()
  {
    // 4x^3 - 3x^2 + 2x - 1
    BEISPIEL_1 = new Polynom(-1.0, 2.0, -3.0, 4.0);
    
    // -4.0x^3
    BEISPIEL_2 = new Polynom(0, 0, 0, -4);
    
    // 3.0x^2 + x
    BEISPIEL_3 = new Polynom(0, 1, 3);
    
    // 3.0x^2 - x
    BEISPIEL_4 = new Polynom(0, -1, 3);
    
    // 1
    BEISPIEL_5 = new Polynom(1);
    
    // -1
    BEISPIEL_6 = new Polynom(-1);
    
    // 0
    BEISPIEL_7 = new Polynom(0, 0, 0, 0);
    
    // (1/3)x^3 - 2x^2 + 3x
    BEISPIEL_8 = new Polynom(0, 3, -2, 1.0 / 3.0);
    
    // x^2 - 4.0x + 3.0
    BEISPIEL_9 = new Polynom(3, -4, 1);
    
    // 2.0x - 4.0
    BEISPIEL_10 = new Polynom(-4, 2);
    
    // 3x^2 + 3x
    BEISPIEL_11 = new Polynom(0, 3, 3);
    
    // -4.0x^3 + 3.0x^2 + x + 1.0
    BEISPIEL_12 = new Polynom(1, 1, 3, -4);
    
    // -3.0x^3 - 5.0x^2 + 2.0x + 1.0
    BEISPIEL_13 = new Polynom(1, 2, -5, -3);
    
  }
  
  /**
   * Test method for {@link de.htw.pbottin.Polynom#berechnen(double)}.
   */
  @Test
  public void testBerechne()
  {
    assertEquals(-1.0, BEISPIEL_1.berechnen(0), 0.0);
    assertEquals(2.0, BEISPIEL_1.berechnen(1), 0.0);
    assertEquals(47.75, BEISPIEL_1.berechnen(2.5), 0.0);
    assertEquals(175.25, BEISPIEL_1.berechnen(3.75), 0.0);
  }
  
  /**
   * Test method for {@link de.htw.pbottin.Polynom#toString()}.
   */
  @Test
  public void testToString()
  {
    assertEquals("4.0x^3-3.0x^2+2.0x-1.0", BEISPIEL_1.toString());
    assertEquals("-4.0x^3", BEISPIEL_2.toString());
    assertEquals("3.0x^2+x", BEISPIEL_3.toString());
    assertEquals("3.0x^2-x", BEISPIEL_4.toString());
    assertEquals("1.0", BEISPIEL_5.toString());
    assertEquals("-1.0", BEISPIEL_6.toString());
    assertEquals("0.0", BEISPIEL_7.toString());
  }
  
  /**
   * Test method for {@link de.htw.pbottin.Polynom#addieren(Polynom)}.
   */
  @Test
  public void testAddieren()
  {
    assertEquals("2.0x^3-3.0x^2", BEISPIEL_1.addieren(new Polynom(1, -2, 0, -2)).toString());
    // assertEquals("", polynom.addieren(new Polynom()).toString());
    // assertEquals("", polynom.addieren(new Polynom()).toString());
  }
  
  /**
   * Test method for {@link de.htw.pbottin.Polynom#subtrahieren(Polynom)}.
   */
  @Test
  public void testSubtrahieren()
  {
    assertEquals("2.0x^3-3.0x^2", BEISPIEL_1.subtrahieren(new Polynom(-1, 2, 0, 2)).toString());
    // assertEquals("", polynom.subtrahieren(new Polynom()).toString());
    // assertEquals("", polynom.subtrahieren(new Polynom()).toString());
  }
  
  /**
   * Test method for {@link de.htw.pbottin.Polynom#ableiten()}.
   */
  @Test
  public void testAbleiten()
  {
    assertEquals("x^2-4.0x+3.0", BEISPIEL_8.ableiten().toString());
    assertEquals("2.0x-4.0", BEISPIEL_9.ableiten().toString());
    assertEquals("2.0", BEISPIEL_10.ableiten().toString());
  }
  
  /**
   * Test method for {@link de.htw.pbottin.Polynom#multiplizieren(Polynom)}.
   */
  @Test
  public void testMultiplizieren()
  {
    // ( 4x^3 - 3x^2 + 2x - 1 ) * ( 3x^2 + 3x ) = 12x^5+3x^4-3x^3+3x^2-3x
    assertEquals("12.0x^5+3.0x^4-3.0x^3+3.0x^2-3.0x", BEISPIEL_1.multiplizieren(BEISPIEL_11).toString());
    
    assertEquals("1.0", BEISPIEL_5.multiplizieren(BEISPIEL_5).toString());
    assertEquals("-1.0", BEISPIEL_5.multiplizieren(BEISPIEL_6).toString());
    assertEquals("0.0", BEISPIEL_5.multiplizieren(BEISPIEL_7).toString());
    
    if (!Polynom.TEST_GRAD)
      // (-4.0x^3+3.0x^2+x+1.0) * (-3.0x^3-5.0x^2+2.0x+1.0) = 12.0x^6+11.0x^5-26.0x^4-6.0x^3+3.0x+1.0
      assertEquals("12.0x^6+11.0x^5-26.0x^4-2.0x^3-3.0x^2+2.0x", BEISPIEL_12.multiplizieren(BEISPIEL_13).toString());
  }
  
  /**
   * Test method for {@link de.htw.pbottin.Polynom#horner(double)}.
   */
  @Test
  public void testHorner()
  {
    assertEquals("4.0x^2+5.0x+12.0", BEISPIEL_1.horner(2).toString());
  }
  
}
