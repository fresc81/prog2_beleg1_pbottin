package de.htw.pbottin;

import java.util.Arrays;
import java.util.Collection;
import static java.lang.Math.*;

/**
 * 
 * Klasse für die Darstellung und Berechnung von Polynomen (Ganzrationale
 * Funktionen N-ten Grades).
 * 
 * @author Paul Bottin
 * 
 */
public class Polynom
{
  
  /**
   * Konfiguriert, ob diese Klasse nur Gradwerte innerhalb des Intervalls
   * <code>[0;MAX_GRAD]</code> unterstützt.
   */
  public static final boolean TEST_GRAD = true;
  
  /**
   * 
   */
  public static final int MAX_GRAD = 6;
  
  /**
   * Die Koeffizienten dieses Polynoms. Das Array enthÃ¤lt mindestens ein
   * Element, auch wenn dieses den Wert 0.0 hat.
   */
  private final double[] koeffizienten;
  
  /**
   * Erstellt eine Instanz der Polynom-Klasse.
   * 
   * @param koeffizienten
   *          die Koeffizienten des Polynoms in aufsteigender Reihenfolge.
   * @throws IllegalArgumentException
   *           wenn der Grad des Polynoms nicht im Bereich <code>[0;6]</code>
   *           liegt
   */
  @SuppressWarnings("unused")
  public Polynom(double... koeffizienten)
  {
    this.koeffizienten = normalisieren(koeffizienten);
    if (TEST_GRAD && getGrad() > MAX_GRAD)
      throw new IllegalArgumentException("Grad des Polynoms ist zu hoch.");
  }
  
  /**
   * Erstellt eine Instanz der Polynom-Klasse.
   * 
   * @param koeffizienten
   *          die Koeffizienten des Polynoms in aufsteigender Reihenfolge.
   * @throws IllegalArgumentException
   *           wenn der Grad des Polynoms nicht im Bereich <code>[0;6]</code>
   *           liegt
   */
  @SuppressWarnings("unused")
  public Polynom(Collection<Double> koeffizienten)
  {
    double[] tmp = new double[koeffizienten.size()];
    int i = 0;
    for (double koeffizient : koeffizienten)
      tmp[i++] = koeffizient;
    this.koeffizienten = normalisieren(tmp);
    if (TEST_GRAD && getGrad() > MAX_GRAD)
      throw new IllegalArgumentException("Grad des Polynoms ist zu hoch.");
    
  }
  
  /**
   * Normalisiert ein Koeffizienten Array, in dem alle Elemente gleich 0.0 vom
   * Ende des Arrays entfernt werden. Das Array enthält jedoch mindestens ein
   * Element.
   * 
   * @param koeffizienten
   *          das Koeffizienten Array
   * @return das normalisierte Koeffizienten Array
   */
  private static double[] normalisieren(double[] koeffizienten)
  {
    int grad = 0;
    double[] result;
    if (koeffizienten.length == 0)
      result = new double[] { 0.0 };
    else
    {
      for (int i = 0; i < koeffizienten.length; i++)
        if (koeffizienten[i] != 0.0)
          grad = i;
      result = Arrays.copyOf(koeffizienten, grad + 1);
    }
    return result;
  }
  
  /**
   * Berechnet das Polynom an der Stelle <code>x</code>.
   * 
   * @param x
   *          die X-Koordinate
   * @return die berechnete Y-Koordinate
   */
  public double berechnen(double x)
  {
    double ergebnis = 0;
    for (int i = 0; i < koeffizienten.length; i++)
      ergebnis += getKoeffizient(i) * pow(x, i);
    return ergebnis;
  }
  
  /**
   * Addiert das übergebene Polynom zu diesem Polynom und gibt das Ergebnis als
   * neue Polynom-Instanz zurück.
   * 
   * @param rechteSeite
   *          das zu addierende Polynom.
   * @return das Ergebnis der Addition
   */
  public Polynom addieren(Polynom rechteSeite)
  {
    double[] ergebnisKoeffizienten = new double[max(this.koeffizienten.length, rechteSeite.koeffizienten.length)];
    for (int i = 0; i < ergebnisKoeffizienten.length; i++)
      ergebnisKoeffizienten[i] = getKoeffizient(i) + rechteSeite.getKoeffizient(i);
    return new Polynom(ergebnisKoeffizienten);
  }
  
  /**
   * Subtrahiert das übergebene Polynom von diesem Polynom und gibt das Ergebnis
   * als neue Polynom-Instanz zurück.
   * 
   * @param rechteSeite
   *          das zu subtrahierende Polynom.
   * @return das Ergebnis der Subtraktion
   */
  public Polynom subtrahieren(Polynom rechteSeite)
  {
    double[] ergebnisKoeffizienten = new double[max(this.koeffizienten.length, rechteSeite.koeffizienten.length)];
    for (int i = 0; i < ergebnisKoeffizienten.length; i++)
      ergebnisKoeffizienten[i] = getKoeffizient(i) - rechteSeite.getKoeffizient(i);
    return new Polynom(ergebnisKoeffizienten);
  }
  
  /**
   * Ermittelt die 1. Ableitung des Polynoms.
   * 
   * @return die 1. Ableitung des Polynoms
   * @throws IllegalStateException
   *           wenn das Polynom nicht abgeleitet werden kann.
   */
  public Polynom ableiten()
  {
    if (this.koeffizienten.length <= 1)
      throw new IllegalStateException("Grad des Polynoms ist zu niedrig.");
    double[] ergebnisKoeffizienten = new double[this.koeffizienten.length - 1];
    for (int i = 0, j = 1; i < ergebnisKoeffizienten.length; i++, j++)
      ergebnisKoeffizienten[i] = (double) j * getKoeffizient(j);
    return new Polynom(ergebnisKoeffizienten);
  }
  
  /**
   * Multipliziert dieses Polynom mit dem übergebenen Polynom und gibt das
   * Ergebnis als neue Polynom Instanz zurück.
   * 
   * @param rechteSeite
   *    das zu multiplizierende Polynom
   * @return das Ergebnis der Multiplikation
   */
  public Polynom multiplizieren(Polynom rechteSeite)
  {
    double ergebnisKoeffizienten[] = new double[getGrad() + rechteSeite.getGrad()];
    for (int s = 0; s < ergebnisKoeffizienten.length; s++)
    {
      double koeffizient = 0.0;
      for (int r = 0; r <= s; r++)
        koeffizient += getKoeffizient(r) * rechteSeite.getKoeffizient(s - r);
      ergebnisKoeffizienten[s] = koeffizient;
    }
    return new Polynom(ergebnisKoeffizienten);
  }
  
  /**
   * Gibt den Koeffizienten mit dem angegebenen Index zurück.
   * 
   * @param i
   *          der Index des Koeffizienten
   * @return der Koeffizient mit dem Index <code>i</code>
   */
  public double getKoeffizient(int i)
  {
    double result;
    if (i < 0)
      throw new IllegalArgumentException();
    if (i >= koeffizienten.length)
      result = 0.0;
    else
      result = koeffizienten[i];
    return result;
  }
  
  /**
   * Berechnet die Division dieses Polynom durch <code>(x-a)</code> nach dem
   * Horner Schema.
   * 
   * @param a
   *          konstanter Faktor a
   * @return das Ergebnispolynom
   */
  public Polynom horner(double a)
  {
    final int ergebnisGrad = getGrad();
    final double ergebnisKoeffizienten[] = new double[ergebnisGrad+1];
    double iterationsVariable = 0;
    for (int i = 0; i < ergebnisKoeffizienten.length; i++)
    {
      iterationsVariable = getKoeffizient(ergebnisGrad-i) + iterationsVariable * a;
      ergebnisKoeffizienten[ergebnisGrad-i] = iterationsVariable;
    }
    return new Polynom(Arrays.copyOfRange(ergebnisKoeffizienten, 1, ergebnisKoeffizienten.length-1));
  }
  
  /**
   * Gibt den Grad dieses Polynoms zurück.
   * 
   * @return der Grad des Polynoms
   */
  public int getGrad()
  {
    return ((koeffizienten.length == 1) && (getKoeffizient(0) == 0.0)) ? 0 : koeffizienten.length;
  }
  
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    boolean start = true;
    double koeffizient;
    for (int i = koeffizienten.length - 1; i >= 0; i--)
    {
      koeffizient = getKoeffizient(i);
      if ((koeffizient != 0.0) || (koeffizienten.length == 1))
      {
        if (koeffizient >= 0.0 && !start)
          sb.append("+");
        
        if (i == 0)
          sb.append(koeffizient);
        else if (koeffizient == -1.0)
          sb.append("-");
        else if (koeffizient != 1.0)
          sb.append(koeffizient);
        
        if (i > 0)
        {
          sb.append("x");
          
          if (i > 1)
          {
            sb.append("^");
            sb.append(i);
          }
        }
        
      }
      start = false;
    }
    return sb.toString();
  }
  
}
