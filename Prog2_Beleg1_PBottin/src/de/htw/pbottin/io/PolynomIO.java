package de.htw.pbottin.io;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;
import de.htw.pbottin.Polynom;

/**
 * Diese Klasse stellt Methoden für die Ein- und Ausgabe des Polynomrechners zur Verfügung.
 * 
 * @author Paul Bottin
 */
public class PolynomIO
{
  
  /**
   * ein Muster, dem Bezeichner entsprechen müssen
   */
  private static final Pattern BEZEICHNER_PATTERN = Pattern.compile("^[a-zA-Z][a-zA-Z0-9]*$", Pattern.MULTILINE);
  
  /**
   * die Spaltenbreite für Wertetabellen
   */
  private static final int TABELLE_SPALTENBREITE = 16;
  
  /**
   * ein Scanner, der aus der Eingabe liest
   */
  private final Scanner input;
  
  /**
   * ein Printer, der in die Ausgabe schreibt
   */
  private final PrintStream output;
  
  /**
   * Erstellt ein PolynomIO-Objekt und leitet die Ein- und Ausgabe um.
   * @param input der Eingabedatenstrom
   * @param output der Ausgabedatenstrom
   */
  public PolynomIO(InputStream input, OutputStream output)
  {
    this.input = new Scanner(input == null ? System.in : input);
    this.output = new PrintStream(output == null ? System.out : output);
  }
  
  /**
   * Erstellt ein PolynomIO-Objekt und benutzt die Standardein- und -ausgabe.
   */
  public PolynomIO()
  {
    this(System.in, System.out);
  }
  
  /**
   * Gibt den Scanner für die Eingabe zurück.
   * @return der Scanner für die Eingabe
   */
  public Scanner getInput()
  {
    return input;
  }
  
  /**
   * Gibt den Printer für die Ausgabe zurück.
   * @return der Printer für die Ausgabe
   */
  public PrintStream getOutput()
  {
    return output;
  }
  
  /**
   * Liest einen Bezeichner aus der Eingabe.
   * @return der Bezeichner
   */
  public String bezeichnerEingeben()
  {
    String bezeichner = null;
    do
    {
      output.print("Bitte geben Sie einen Bezeichner für das Polynom ein: ");
      if (!input.hasNext(BEZEICHNER_PATTERN))
      {
        output.println("Ungültige Eingabe, bitte erneut versuchen.");
        input.next();
      } else
        bezeichner = input.next(BEZEICHNER_PATTERN);
    } while (bezeichner == null);
    return bezeichner;
  }
  
  /**
   * Liest ein Polynom aus der Eingabe.
   * @return das Polynom
   */
  public Polynom polynomEingeben()
  {
    output.println("Eingabe in der Form a[0] + a[1]*x + ... + a[n]*x^n");
    int numKoeff = intWertEingeben("Wie viele Koeffizienten (a) möchten Sie eingeben? ");
    double[] koeff = new double[numKoeff];
    for (int i = 0; i < numKoeff; i++)
      koeff[i] = doubleWertEingeben(String.format("Koeffizient a[%d]: ", i));
    return new Polynom(koeff);
  }
  
  /**
   * Liest einen <code>int</code>-Wert ein.
   * @param prompt die anzuzeigende Meldung
   * @return der <code>int</code>-Wert
   */
  public int intWertEingeben(String prompt)
  {
    Integer wert = null;
    do
    {
      output.print(prompt);
      if (!input.hasNextInt())
      {
        output.println("Ungültige Eingabe, bitte erneut versuchen.");
        input.next();
      } else
        wert = input.nextInt();
    } while (wert == null);
    return wert;
  }
  
  /**
   * Liest einen <code>double</code>-Wert ein.
   * @param prompt die anzuzeigende Meldung
   * @return der <code>double</code>-Wert
   */
  public double doubleWertEingeben(String prompt)
  {
    Double wert = null;
    do
    {
      output.print(prompt);
      if (!input.hasNextDouble())
      {
        output.println("Ungültige Eingabe, bitte erneut versuchen.");
        input.next();
      } else
        wert = input.nextDouble();
    } while (wert == null);
    return wert;
  }
  
  /**
   * Gibt eine Wertetabelle aus.
   * @param polynom das Polynom, für das die Wertetabelle ausgegeben werden soll.
   * @param xmin der Startwert
   * @param xmax der Endwert
   * @param xstep die Schrittweite
   */
  public void wertetabelleAusgeben(Polynom polynom, double xmin, double xmax, double xstep)
  {
    output.printf("|%-"+TABELLE_SPALTENBREITE+"s|%-"+TABELLE_SPALTENBREITE+"s|\n", "x", "y");
    char[] hline = new char[TABELLE_SPALTENBREITE];
    Arrays.fill(hline, '-');
    output.printf("+%s+%s+\n", String.valueOf(hline), String.valueOf(hline));
    for (double x = xmin; x <= xmax; x += xstep)
      output.printf("|%"+TABELLE_SPALTENBREITE+"s|%"+TABELLE_SPALTENBREITE+"s|\n", x, polynom.berechnen(x));
  }
  
}
