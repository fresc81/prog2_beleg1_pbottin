/**
 * 
 */
package de.htw.pbottin.io;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Diese Klasse stellt Methoden zum Anzeigen und Verwalten von Menüs in der Konsole bereit.<br>
 * Wahlweise kann die Ein-/Ausgabe umgeleitet werden um zum Beispiel Menüs über eine TCP-Verbindung
 * zu realisieren. Die Klasse arbeitet eng mit der eingenisteten Klasse {@link Menu.Menupunkt} zusammen.
 * @author Paul Bottin
 * @param <T> Elementtyp, der von den Menüpunkten zur Speicherung des Menüelements benutzt wird.
 */
public class Menu<T>
{
  
  /**
   * Diese Klasse kapselt einen Menüpunkt.<br>
   * Optional kann ein Elementtyp angegeben werden, der von dem Menüpunkt representiert wird. Wenn
   * dieser nicht benötigt wird, sollte {@link Void} verwendet werden.<br>
   * Jedem Menüpunkt muss ein Text oder ein Element zugeordnet werden. Dieser Text bzw. der
   * Rückgabewert der {@link Object#toString()}-Methode des Elements wird als Beschreibung für
   * den Menüpunkt angezeigt.<br>
   * Jedem Menüpunkt muss ein Bezeichner zugeordnet werden. Mit diesem Bezeichner oder der Nummer des
   * Eintrags kann der Benutzer einen Menüpunkt auswählen.<br>
   * @author Paul Bottin
   * @param <T> Elementtyp, der von diesem Menüpunkt zur Speicherung des Menüelements benutzt wird.
   */
  public static class Menupunkt<T>
  {
    
    /**
     * das Element bzw. <code>null</code>, wenn nur Text angegeben wurde.
     */
    private T element; 
    
    /**
     * eine kurze Beschreibung des Menüpunkts
     */
    private String text; 
    
    /**
     * ein möglichst eindeutiger Bezeichner für den Menüpunkt
     */
    private final String bezeichner;
    
    /**
     * ein Muster welches benutzt wird, um die Eingabe des Benutzers
     * zu prüfen (Standardmäßig der Bezeichner)
     */
    private final Pattern pattern;
    
    /**
     * Erstellt einen Menüpunkt unter Angabe eines Menüelements.
     * @param element das Menüelement
     * @param bezeichner der Bezeichner
     * @param pattern das Muster
     */
    public Menupunkt(T element, String bezeichner, Pattern pattern)
    {
      this(element.toString(), bezeichner, pattern);
      this.element = element;
    }
    
    /**
     * Erstellt einen Menüpunkt unter Angabe eines Textes. Das Menüelement ist <code>null</code>.
     * @param text der Text des Menüpunktes
     * @param bezeichner der Bezeichner
     * @param pattern das Muster
     */
    public Menupunkt(String text, String bezeichner, Pattern pattern)
    {
      if (bezeichner == null)
        throw new NullPointerException("bezeichner darf nicht null sein");
      if (bezeichner.isEmpty())
        throw new IllegalArgumentException("bezeichner darf nicht leer sein");
      
      if (text == null)
        throw new NullPointerException("text darf nicht null sein");
      if (text.isEmpty())
        throw new IllegalArgumentException("text darf nicht leer sein");
      
      this.text = text;
      this.bezeichner = bezeichner;
      this.pattern = pattern == null ? Pattern.compile(bezeichner) : pattern;
    }
    
    /**
     * Gibt den Text des Menüpunktes zurück.
     * @return der Text des Menüpunktes
     */
    public String getText()
    {
      return text;
    }
    
    /**
     * Setzt den Text des Menüpunktes. Das Menüelement wird auf <code>null</code> gesetzt.
     * @param text der neue Text des Menüpunktes
     */
    public void setText(String text)
    {
      this.element = null;
      this.text = text;
    }
    
    /**
     * Setzt das Menüelement.
     * @param element das Menüelement
     */
    public void setElement(T element)
    {
      this.element = element;
      this.text = element.toString();
    }
    
    /**
     * Gibt das Menüelement zurück.
     * @return das Menüelement, bzw. <code>null</code> wenn nur ein Text angegeben wurde.
     */
    public T getElement()
    {
      return element;
    }
    
    /**
     * Gibt den Bezeichner zurück.
     * @return der Bezeicher
     */
    public String getBezeichner()
    {
      return bezeichner;
    }
    
    /**
     * Gibt das Muster zurück, welches benutzt wird um die Eingabe des Benutzers
     * zu prüfen.
     * @return das Muster
     */
    public Pattern getPattern()
    {
      return pattern;
    }
    
    /**
     * Aktion die ausgeführt werden soll, wenn der Benutzer diesen Menüpunkt ausgewählt hat.<br>
     * Standardmäßig ist diese Methode leer. Sie kann in einer Ableitung dieser Klasse überschrieben
     * werden, um eine Aktion zu definieren.
     */
    protected void aktion()
    {
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
      return String.format("%-48s (%s)", getText(), getBezeichner());
    }
    
  }
  
  /**
   * die Menüpunkte dieses Menüs
   */
  private final ArrayList<Menupunkt<T>> menupunkte = new ArrayList<>();
  
  /**
   * der Eingabe-Scanner, liest standardmäßig von der Standardeingabe
   */
  private final Scanner input;
  
  /**
   * der Ausgabe-Printer, schreibt standardmäßig in die Standardausgabe
   */
  private final PrintStream output;
  
  /**
   * der Menütitel
   */
  private String menutitel = null;
  
  /**
   * Erstellt ein leeres Menü.
   */
  public Menu()
  {
    this(System.in, System.out);
  }
  
  /**
   * Erstellt ein leeres Menü und leitet die Ein-/Ausgabe um.
   * @param input die Eingabe
   * @param output die Ausgabe
   */
  public Menu(InputStream input, OutputStream output)
  {
    this.input = new Scanner(input == null ? System.in : input);
    this.output = new PrintStream(output == null ? System.out : output);
  }
  
  /**
   * Erstellt ein Menü mit den angegebenen Menüpunkten.
   * @param menupunkte die Menüpunkte
   */
  public Menu(Collection<Menupunkt<T>> menupunkte)
  {
    this(System.in, System.out);
    getMenupunkte().addAll(menupunkte);
  }
  
  /**
   * Erstellt ein Menü mit den angegebenen Menüpunkten und leitet die Ein-/Ausgabe um.
   * @param input die Eingabe
   * @param output die Ausgabe
   * @param menupunkte die Menüpunkte
   */
  public Menu(InputStream input, OutputStream output, Collection<Menupunkt<T>> menupunkte)
  {
    this(input, output);
    getMenupunkte().addAll(menupunkte);
  }
  
  /**
   * Erstellt ein Menü mit den angegebenen Menüpunkten.
   * @param menupunkte die Menüpunkte
   */
  @SafeVarargs
  public Menu(Menupunkt<T>... menupunkte)
  {
    this(System.in, System.out);
    getMenupunkte().addAll(Arrays.asList(menupunkte));
  }
  
  /**
   * Erstellt ein Menü mit den angegebenen Menüpunkten und leitet die Ein-/Ausgabe um.
   * @param input die Eingabe
   * @param output die Ausgabe
   * @param menupunkte die Menüpunkte
   */
  @SafeVarargs
  public Menu(InputStream input, OutputStream output, Menupunkt<T>... menupunkte)
  {
    this(input, output);
    getMenupunkte().addAll(Arrays.asList(menupunkte));
  }
  
  /**
   * Zeigt das Menü an und fordert den Benutzer zur Auswahl eines Menüpunktes auf.<br>
   * Gibt den ausgewählten Menüpunkt zurück.
   * @return der ausgewählte Menüpunkt
   */
  public Menupunkt<T> anzeigen()
  {
    int i;
    String auswahl;
    Menupunkt<T> ergebnis = null;
    if (getMenupunkte().isEmpty())
      throw new IllegalStateException("das Menü ist leer");
    do
    {
      if (getMenutitel() != null)
        getOutput().printf("%s\n", getMenutitel());
      i = 1;
      for (Menupunkt<T> menupunkt : getMenupunkte())
        getOutput().printf("%2d. %s\n", i++, menupunkt.toString());
      getOutput().println("\nBitte geben Sie Ihre Auswahl ein (Nummer oder die Zeichenkette in der Klammer):");
      
      if (getInput().hasNextLine())
      {
        i = 1;
        auswahl = getInput().nextLine().trim();
        for (Menupunkt<T> menupunkt : getMenupunkte())
        {
          if (auswahl.equals(Integer.toString(i)) || menupunkt.getPattern().matcher(auswahl).matches())
          {
            ergebnis = menupunkt;
            menupunkt.aktion();
          }
          ++i;
        }
      }
      
      if (ergebnis == null)
        getOutput().println("Ungültige Eingabe! Bitte erneut versuchen.");
      
    } while (ergebnis == null);
    return ergebnis;
  }
  
  /**
   * Gibt den Titel des Menüs zurück.
   * @return der Menütitel
   */
  public String getMenutitel()
  {
    return menutitel;
  }
  
  /**
   * Setzt den Menütitel.
   * @param menutitel der Menütitel
   */
  public void setMenutitel(String menutitel)
  {
    this.menutitel = menutitel;
  }
  
  /**
   * Gibt die Liste der Menüelpunkte zurück.
   * @return die Liste der Menüpunkte
   */
  public List<Menupunkt<T>> getMenupunkte()
  {
    return menupunkte;
  }
  
  /**
   * Gibt den Scanner für die Eingaben zurück.
   * @return der Scanner für die Eingaben
   */
  public Scanner getInput()
  {
    return input;
  }
  
  /**
   * Gibt den Printer für die Ausgaben zurück.
   * @return der Printer für die Ausgaben
   */
  public PrintStream getOutput()
  {
    return output;
  }
  
  /**
   * Findet den erstem Menüpunkt mit dem angegebenen Bezeichner. Gibt <code>null</code> zurück, wenn
   * kein Menüpunkt mit dem angegebenen Bezeichner existiert.
   * @param bezeichner der Bezeichner
   * @return der erste Menüpunkt mit dem angegebenen Bezeichner
   */
  public Menupunkt<T> findeElement(String bezeichner)
  {
    Menupunkt<T> gefunden = null;
    for (Menupunkt<T> menupunkt : getMenupunkte())
    {
      if ((menupunkt.getElement() != null) && menupunkt.getBezeichner().equals(bezeichner))
      {
        gefunden = menupunkt;
        break;
      }
    }
    return gefunden;
  }
  
}
