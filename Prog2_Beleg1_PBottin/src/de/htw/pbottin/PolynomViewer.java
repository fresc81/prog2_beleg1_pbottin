package de.htw.pbottin;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JButton;
import java.awt.Canvas;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Dialog, der ein Polynom anzeigt (bzw. plottet).
 * Das Layout wurde mit einem Fenster-Designer erstellt.
 * 
 * @author Paul Bottin
 */
public class PolynomViewer extends JDialog
{
  
  /**
   * Seriennummer
   * @see java.io.Serializable
   */
  private static final long serialVersionUID = 316863550902793938L;
  
  /**
   * Das zu plottende Polynom.
   */
  private Polynom polynom = new Polynom(0);
  
  /**
   * Erstellt den Dialog.
   * @param polynom das zu plottende Polynom
   */
  public PolynomViewer(Polynom polynom)
  {
    this();
    setPolynom(polynom);
  }
  
  /**
   * Erstellt den Dialog.
   */
  public PolynomViewer()
  {
    setTitle("Polynom Viewer");
    setBounds(100, 100, 450, 300);
    
    JPanel panel = new JPanel();
    getContentPane().add(panel, BorderLayout.SOUTH);
    panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
    
    JButton btnNewButton = new JButton("Schlie\u00DFen");
    btnNewButton.addActionListener(new ActionListener()
    {
      
      public void actionPerformed(ActionEvent e)
      {
        setVisible(false);
        dispose();
      }
    });
    panel.add(btnNewButton);
    
    JPanel panel_1 = new JPanel();
    panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
    getContentPane().add(panel_1, BorderLayout.CENTER);
    panel_1.setLayout(new BorderLayout(0, 0));
    
    Canvas canvas = new Canvas()
    {
      
      private static final long serialVersionUID = 8159523852157013862L;
      
      /*
       * (non-Javadoc)
       * 
       * @see java.awt.Canvas#paint(java.awt.Graphics)
       */
      @Override
      public void paint(Graphics g)
      {
        super.paint(g);
        
        Graphics2D g2 = (Graphics2D) g;
        paintLabel(g2);
        paintGraph(g2);
        paintGrid(g2);
      }
      
      private void paintLabel(Graphics2D g)
      {
        g.drawString(polynom.toString(), 10, 10);
      }
      
      private void paintGrid(Graphics2D g)
      {
        g.setColor(new Color(0.5f, 0.5f, 0.5f, 0.5f));
        g.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
        g.drawLine(getWidth() / 2, 0, getWidth() / 2, getHeight());
      }
      
      private void paintGraph(Graphics2D g)
      {
        double y, dx = 20.0 / (double) getWidth();
        
        AffineTransform trans = new AffineTransform();
        trans.concatenate(AffineTransform.getTranslateInstance((double) getWidth() / 2.0, (double) getHeight() / 2.0));
        trans.concatenate(AffineTransform.getScaleInstance((double) getWidth() / 20.0, -(double) getHeight() / 100.0));
        
        ArrayList<Point2D> points = new ArrayList<>();
        Point2D point = new Point2D.Double();
        for (double x = -10.0; x <= 10.0; x += dx) // x von -10 bis 10, y von
                                                   // -50 bis 50
        {
          y = polynom.berechnen(x);
          trans.transform(new Point2D.Double(x, y), point);
          points.add((Point2D) point.clone());
        }
        
        int size = points.size();
        int[] xPoints = new int[size];
        int[] yPoints = new int[size];
        for (int i = 0; i < size; i++)
        {
          point = points.get(i);
          xPoints[i] = (int) Math.floor(point.getX());
          yPoints[i] = (int) Math.floor(point.getY());
        }
        g.drawPolyline(xPoints, yPoints, size);
        
      }
      
    };
    
    canvas.setForeground(Color.BLACK);
    canvas.setBackground(Color.WHITE);
    panel_1.add(canvas, BorderLayout.CENTER);
    
  }
  
  /**
   * Das zu plottende Polynom zurückgeben.
   * @return das zu plottende Polynom
   */
  public Polynom getPolynom()
  {
    return polynom;
  }
  
  /**
   * Das zu plottende Polynom setzen.
   * @param polynom das zu plottende Polynom
   */
  public void setPolynom(Polynom polynom)
  {
    this.polynom = polynom;
    repaint();
  }
  
}
