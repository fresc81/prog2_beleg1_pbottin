/**
 * 
 */
package de.htw.pbottin;

import java.util.regex.Pattern;

import javax.swing.JDialog;
import de.htw.pbottin.io.Menu;
import de.htw.pbottin.io.PolynomIO;

/**
 * <h1>Polynomrechner</h1>
 * Interaktive Anwendung, die es erlaubt mit Polynomen zu rechnen.<br>
 * Die Anwendung ist Menügestützt und Benutzerfreundlich. Folgende Features
 * werden unterstützt:
 * <ul>
 *   <li>Eingabe von Polynomen und zuordnen von Bezeichnern für die spätere Verwendung</li>
 *   <li>Addition, Subtraktion, Multiplikation von Polynomen</li>
 *   <li>Ableitung von Polynomen</li>
 *   <li>Berechnung der Division durch (x-a) nach dem Horner-Schema</li>
 *   <li>Plotten von Polynomen mittels Swing GUI</li>
 *   <li>Wertetabellen für Polynome generieren</li>
 * </ul>
 * 
 * @author Paul Bottin
 */
public class PolynomMain
{
  
  /**
   * Objekt, das für die Ein- und Ausgaben zuständig ist.
   */
  private final PolynomIO polynomIO = new PolynomIO();
  
  /**
   * Menü, das den Benutzer fragt, ob das Ergebnis der letzten Operation gespeichert werden soll.
   */
  private final Menu<Boolean > ergebnisSpeichernMenu = new Menu<Boolean >(
  
      // Ja-Menüpunkt
      new Menu.Menupunkt<Boolean>(true, "j", Pattern.compile("[jJ]"))
      {
        
        @Override
        public String getText()
        {
          return "Ja";
        }
        
      },
      
      // Nein-Menüpunkt
      new Menu.Menupunkt<Boolean>(false, "n", Pattern.compile("[nN]"))
      {
        
        @Override
        public String getText()
        {
          return "Nein";
        }
        
      }
  
  );
  
  /**
   * Menü, das zur Auswahl eines gespeicherten Polynoms auffordert.
   */
  private final Menu<Polynom > polynomMenu = new Menu<Polynom >(
      
      // 
      new Menu.Menupunkt<Polynom>("Zurück", "z", Pattern.compile("[zZ]"))
  
  );
  
  /**
   * Das Hauptmenü der Anwendung.
   */
  private final Menu<Void > hauptMenu = new Menu<Void >(
  
      // Lässt den Benutzer ein Polynom eingeben und speichert es unter einem Namen.
      new Menu.Menupunkt<Void>("Polynom eingeben", "e", Pattern.compile("[eE]"))
      {
        
        @Override
        protected void aktion()
        {
          String bezeichner = polynomIO.bezeichnerEingeben();
          Polynom polynom = polynomIO.polynomEingeben();
          Menu.Menupunkt<Polynom> menupunkt = polynomMenu.findeElement(bezeichner);
          if (menupunkt != null)
          {
            menupunkt.setElement(polynom);
            System.out.println("Altes Polynom wurde überschrieben.");
          } else
          {
            menupunkt = new Menu.Menupunkt<Polynom>(polynom, bezeichner, null);
            polynomMenu.getMenupunkte().add(menupunkt);
          }
        }
        
      },
      
      // Addiert zwei vorher eingegebene Polynome.
      new Menu.Menupunkt<Void>("Polynome addieren", "a", Pattern.compile("[aA]"))
      {
        
        @Override
        protected void aktion()
        {
          String menutitel = polynomMenu.getMenutitel();
          polynomMenu.setMenutitel("Erstes Polynom?");
          Menu.Menupunkt<Polynom> auswahl1 = polynomMenu.anzeigen();
          Polynom polynom1 = auswahl1.getElement();
          if (polynom1 != null)
          {
            polynomMenu.setMenutitel("Zweites Polynom?");
            Menu.Menupunkt<Polynom> auswahl2 = polynomMenu.anzeigen();
            Polynom polynom2 = auswahl2.getElement();
            if (polynom2 != null)
              try
              {
                Polynom ergebnis = polynom1.addieren(polynom2);
                System.out.printf("(%s) + (%s) = (%s)\n", polynom1, polynom2, ergebnis);
                frageSpeichern(ergebnis);
              } catch (Exception e)
              {
                System.out.println(e.getLocalizedMessage());
              }
          }
          polynomMenu.setMenutitel(menutitel);
        }
        
      },
      
      // Subtrahiert zwei vorher eingegebene Polynome.
      new Menu.Menupunkt<Void>("Polynome subtrahieren", "s", Pattern.compile("[sS]"))
      {
        
        @Override
        protected void aktion()
        {
          String menutitel = polynomMenu.getMenutitel();
          polynomMenu.setMenutitel("Erstes Polynom?");
          Menu.Menupunkt<Polynom> auswahl1 = polynomMenu.anzeigen();
          Polynom polynom1 = auswahl1.getElement();
          if (polynom1 != null)
          {
            polynomMenu.setMenutitel("Zweites Polynom?");
            Menu.Menupunkt<Polynom> auswahl2 = polynomMenu.anzeigen();
            Polynom polynom2 = auswahl2.getElement();
            if (polynom2 != null)
              try
              {
                Polynom ergebnis = polynom1.subtrahieren(polynom2);
                System.out.printf("(%s) - (%s) = (%s)\n", polynom1, polynom2, ergebnis);
                frageSpeichern(ergebnis);
              } catch (Exception e)
              {
                System.out.println(e.getLocalizedMessage());
              }
          }
          polynomMenu.setMenutitel(menutitel);
        }
        
      },
      
      // Multipliziert zwei vorher eingegebene Polynome.
      new Menu.Menupunkt<Void>("Polynome multiplizieren", "m", Pattern.compile("[mM]"))
      {
        
        @Override
        protected void aktion()
        {
          String menutitel = polynomMenu.getMenutitel();
          polynomMenu.setMenutitel("Erstes Polynom?");
          Menu.Menupunkt<Polynom> auswahl1 = polynomMenu.anzeigen();
          Polynom polynom1 = auswahl1.getElement();
          if (polynom1 != null)
          {
            polynomMenu.setMenutitel("Zweites Polynom?");
            Menu.Menupunkt<Polynom> auswahl2 = polynomMenu.anzeigen();
            Polynom polynom2 = auswahl2.getElement();
            if (polynom2 != null)
              try
              {
                Polynom ergebnis = polynom1.multiplizieren(polynom2);
                System.out.printf("(%s) * (%s) = (%s)\n", polynom1, polynom2, ergebnis);
                frageSpeichern(ergebnis);
              } catch (Exception e)
              {
                System.out.println(e.getLocalizedMessage());
              }
          }
          polynomMenu.setMenutitel(menutitel);
        }
        
      },
      
      // Berechnet die erste Ableitung eines vorher eingegebenen Polynoms.
      new Menu.Menupunkt<Void>("erste Ableitung eines Polynoms berechnen", "d", Pattern.compile("[dD]"))
      {
        
        @Override
        protected void aktion()
        {
          String menutitel = polynomMenu.getMenutitel();
          polynomMenu.setMenutitel("Welches Polynom möchten Sie ableiten?");
          Menu.Menupunkt<Polynom> auswahl = polynomMenu.anzeigen();
          Polynom polynom = auswahl.getElement();
          if (polynom != null)
            try
            {
              Polynom ergebnis = polynom.ableiten();
              System.out.printf("Ableitung(%s) = (%s)\n", polynom, ergebnis);
              frageSpeichern(ergebnis);
            } catch (Exception e)
            {
              System.out.println(e.getLocalizedMessage());
            }
          polynomMenu.setMenutitel(menutitel);
        }
        
      },
      
      // Berechnet die Division durch (x-a) eines vorher eingegebenen Polynoms nach dem Horner Schema.
      new Menu.Menupunkt<Void>("Horner Schema berechnen", "h", Pattern.compile("[hH]"))
      {
        
        @Override
        protected void aktion()
        {
          String menutitel = polynomMenu.getMenutitel();
          polynomMenu.setMenutitel("Auf welches Polynom möchten Sie das Horner Schema anwenden?");
          Menu.Menupunkt<Polynom> auswahl = polynomMenu.anzeigen();
          Polynom polynom = auswahl.getElement();
          if (polynom != null)
            try
            {
              double a = polynomIO.doubleWertEingeben("Wert für a eingeben: ");
              Polynom ergebnis = polynom.horner(a);
              System.out.printf("(%s) / (x-%s) = (%s)\n", polynom, a, ergebnis);
              frageSpeichern(ergebnis);
            } catch (Exception e)
            {
              System.out.println(e.getLocalizedMessage());
            }
          polynomMenu.setMenutitel(menutitel);
        }
        
      },
      
      // Plottet ein vorher eingegebenes Polynom.
      new Menu.Menupunkt<Void>("Polynom anzeigen", "v", Pattern.compile("[vV]"))
      {
        
        @Override
        protected void aktion()
        {
          String menutitel = polynomMenu.getMenutitel();
          polynomMenu.setMenutitel("Welches Polynom möchten Sie anzeigen?");
          Menu.Menupunkt<Polynom> auswahl = polynomMenu.anzeigen();
          Polynom polynom = auswahl.getElement();
          if (polynom != null)
            try
            {
              PolynomViewer dialog = new PolynomViewer(polynom);
              dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
              dialog.setVisible(true);
            } catch (Exception e)
            {
              System.out.println(e.getLocalizedMessage());
            }
          polynomMenu.setMenutitel(menutitel);
        }
        
      },
      
      // Generiert eine Wertetabelle für ein vorher eingegebenes Polynom.
      new Menu.Menupunkt<Void>("Wertetabelle generieren", "w", Pattern.compile("[wW]"))
      {
        
        @Override
        protected void aktion()
        {
          String menutitel = polynomMenu.getMenutitel();
          polynomMenu.setMenutitel("Für welches Polynom möchten Sie eine Wertetabelle generieren?");
          Menu.Menupunkt<Polynom> auswahl = polynomMenu.anzeigen();
          Polynom polynom = auswahl.getElement();
          if (polynom != null)
            try
            {
              polynomIO.wertetabelleAusgeben(polynom, -10, 10, 0.5);
              System.out.println();
            } catch (Exception e)
            {
              System.out.println(e.getLocalizedMessage());
            }
          polynomMenu.setMenutitel(menutitel);
        }
        
      },
      
      // Beendet das Programm.
      new Menu.Menupunkt<Void>("Programm beenden", "x", Pattern.compile("[xX]"))
      {
        
        @Override
        protected void aktion()
        {
          System.exit(0);
        }
        
      }
  
  );
  
  /**
   * Haupteinsprungspunkt in diese Konsolenanwendung
   * @param args Kommandozeilenparameter (werden ignoriert)
   */
  public static void main(String[] args)
  {
    new PolynomMain().starten();
  }
  
  /**
   * Anwendung initialisieren.
   */
  private PolynomMain()
  {
    ergebnisSpeichernMenu.setMenutitel("Möchten Sie das Ergebnis speichern?");
    polynomMenu.setMenutitel("Polynome\n--------");
    hauptMenu.setMenutitel("Hauptmenü\n=========");
  }
  
  /**
   * Fragt den Benutzer, ob er dem übergebenen Polynom einen Bezeichner zuordnen möchte.
   * Das Polynom ist dann für weitere Operationen verfügbar. 
   * @param polynom das zu speichernde Polynom
   */
  private void frageSpeichern(Polynom polynom)
  {
    if (ergebnisSpeichernMenu.anzeigen().getElement())
    {
      String bezeichner = polynomIO.bezeichnerEingeben();
      Menu.Menupunkt<Polynom> menupunkt = polynomMenu.findeElement(bezeichner);
      if (menupunkt != null)
      {
        menupunkt.setElement(polynom);
        System.out.println("Altes Polynom wurde überschrieben.");
      } else
      {
        menupunkt = new Menu.Menupunkt<Polynom>(polynom, bezeichner, null);
        polynomMenu.getMenupunkte().add(menupunkt);
      }
    }
  }
  
  /**
   * Anwendung starten.
   */
  public void starten()
  {
    while (true)
      hauptMenu.anzeigen();
  }
  
}
